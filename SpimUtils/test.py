import SpimUtils


if __name__ == '__main__':
    from timeit import timeit

    ts = [timeit("SpimUtils.fromSpimFolder('../SpimGL/Example',pos=%i,count=1)"%i,
               "import SpimUtils",number=2) for i in range(10)]


    d  = SpimUtils.fromSpimFolder('../SpimGL/Example',pos=0,count=-1)
