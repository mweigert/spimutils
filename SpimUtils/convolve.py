import numpy as np
from PyOCL import OCLDevice, OCLProcessor, cl
from utils import absPath



__all__ = ['convolve2','convolve3','convolve_sep2','convolve_sep3']






def convolve2(dev,data,h):
    """convolves 2d data with kernel h on the GPU Device dev
    boundary conditions are clamping to edge.
    h is converted to float32
    """

    dtype = data.dtype.type

    dtypes_kernels = {np.float32:"convolve2d_float",
                      np.uint16:"convolve2d_short"}

    if not dtype in dtypes_kernels.keys():
        print "data type %s not supported yet, please convert to:"%dtype,dtypes_kernels.keys()
        return

    proc = OCLProcessor(dev,absPath("kernels/convolve_kernels.cl"))

    Ny,Nx = h.shape

    hbuf = dev.createBuffer(Nx*Ny,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)
    inImg = dev.createImage(data.shape[::-1],dtype = dtype )
    outImg = dev.createImage(data.shape[::-1],dtype = dtype,mem_flags= cl.mem_flags.WRITE_ONLY)

    dev.writeImage(inImg,data)

    dev.writeBuffer(hbuf,h.astype(np.float32).flatten())


    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,inImg,hbuf,np.int32(Nx),np.int32(Ny),outImg)


    return dev.readImage(outImg)


def convolve3(dev,data,h):
    """convolves 3d data with kernel h on the GPU Device dev
    boundary conditions are clamping to edge.
    h is converted to float32
    """

    dtype = data.dtype.type

    dtypes_kernels = {np.float32:"convolve3d_float",
                      np.uint16:"convolve3d_short"}

    if not dtype in dtypes_kernels.keys():
        print "data type %s not supported yet, please convert to:"%dtype,dtypes_kernels.keys()
        return

    proc = OCLProcessor(dev,absPath("kernels/convolve_kernels.cl"))

    Nz,Ny,Nx = h.shape
    hbuf = dev.createBuffer(Nx*Ny*Nz,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)
    inImg = dev.createImage(data.shape[::-1],dtype = dtype )
    outImg = dev.createImage(data.shape[::-1],dtype = dtype,mem_flags= cl.mem_flags.WRITE_ONLY)

    dev.writeImage(inImg,data)

    dev.writeBuffer(hbuf,h.astype(np.float32).flatten())

    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,inImg,hbuf,np.int32(Nx),np.int32(Ny),np.int32(Nz),outImg)


    return dev.readImage(outImg)


def convolve_sep2(dev,data,hx,hy):
    """convolves 2d data with kernel h = outer(hx,hy) on the GPU Device dev
    boundary conditions are clamping to edge.
    hx, hy are  converted to float32
    """

    dtype = data.dtype.type

    dtypes_kernels = {np.float32:"convolve_sep2d_float",
                      np.uint16:"convolve_sep2d_short"}

    if not dtype in dtypes_kernels.keys():
        print "data type %s not supported yet, please convert to:"%dtype,dtypes_kernels.keys()
        return


    proc = OCLProcessor(dev,absPath("kernels/convolve_kernels.cl"))

    Ny,Nx = len(hy),len(hx)

    hxbuf = dev.createBuffer(Nx,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)
    hybuf = dev.createBuffer(Ny,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)

    inImg = dev.createImage(data.shape[::-1],dtype = dtype )
    outImg = dev.createImage(data.shape[::-1],dtype = dtype,mem_flags= cl.mem_flags.WRITE_ONLY)
    tmpImg = dev.createImage(data.shape[::-1],dtype = dtype,mem_flags= cl.mem_flags.READ_WRITE)

    dev.writeImage(inImg,data)

    dev.writeBuffer(hxbuf,hx.astype(np.float32).flatten())
    dev.writeBuffer(hybuf,hy.astype(np.float32).flatten())


    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,inImg,hxbuf,np.int32(Nx),tmpImg,np.int32(1))
    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,tmpImg,hybuf,np.int32(Ny),outImg,np.int32(2))


    return dev.readImage(outImg)

def convolve_sep3(dev,data,hx,hy,hz):
    """convolves 3d data with kernel h = outer(hx,hy,hz) on the GPU Device dev
    boundary conditions are clamping to edge.
    hx, hy, hz are converted to float32
    """

    dtype = data.dtype.type

    dtypes_kernels = {np.float32:"convolve_sep3d_float",
                      np.uint16:"convolve_sep3d_short"}

    if not dtype in dtypes_kernels.keys():
        print "data type %s not supported yet, please convert to:"%dtype,dtypes_kernels.keys()
        return


    proc = OCLProcessor(dev,absPath("kernels/convolve_kernels.cl"))

    Nz, Ny,Nx = len(hz),len(hy),len(hx)

    hxbuf = dev.createBuffer(Nx,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)
    hybuf = dev.createBuffer(Ny,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)
    hzbuf = dev.createBuffer(Nz,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)

    inImg = dev.createImage(data.shape[::-1],dtype = dtype )
    outImg = dev.createImage(data.shape[::-1],dtype = dtype,mem_flags= cl.mem_flags.READ_WRITE)
    tmpImg = dev.createImage(data.shape[::-1],dtype = dtype,mem_flags= cl.mem_flags.READ_WRITE)

    dev.writeImage(inImg,data)

    dev.writeBuffer(hxbuf,hx.astype(np.float32).flatten())
    dev.writeBuffer(hybuf,hy.astype(np.float32).flatten())
    dev.writeBuffer(hzbuf,hz.astype(np.float32).flatten())


    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,inImg,hxbuf,np.int32(Nx),outImg,np.int32(1))
    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,outImg,hybuf,np.int32(Ny),tmpImg,np.int32(2))
    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,tmpImg,hzbuf,np.int32(Nz),outImg,np.int32(4))


    return dev.readImage(outImg)



def test_convolve2():
    dev = OCLDevice(useDevice=1)
    data  = 10*np.ones((100,100))
    h = np.ones((30,30))

    #float
    out = convolve2(dev,data.astype(np.float32),h)
    np.testing.assert_equal(out[40,40],data[40,40]*np.sum(h))

    #short
    out = convolve2(dev,data.astype(np.uint16),h)
    np.testing.assert_equal(out[40,40],data[40,40]*np.sum(h))


def test_convolve_sep2():
    dev = OCLDevice(useDevice=1)
    data  = 10*np.ones((100,100))
    hx = np.ones(10)
    hy = 2.*np.ones(10)

    #float
    out = convolve_sep2(dev,data.astype(np.float32),hx,hy)
    np.testing.assert_equal(out[40,40],data[40,40]*np.sum(hx)*np.sum(hy))

    #short
    out = convolve_sep2(dev,data.astype(np.uint16),hx,hy)
    np.testing.assert_equal(out[40,40],data[40,40]*np.sum(hx)*np.sum(hy))



def test_convolve3():
    from time import time

    dev = OCLDevice(useDevice=1)
    data  = 10*np.ones((256,)*3,dtype=np.float32)
    h = np.ones((20,20,20))

    sumTime = 0
    for dtype in (np.float32,np.uint16):
        t = time()
        out = convolve3(dev,data.astype(dtype),h)
        sumTime += time()-t
        np.testing.assert_equal(out[40,40,40],data[40,40,40]*np.sum(h))

    print "convolving %s image with %s kernel in %.2f ms"%(data.shape,h.shape,1000*sumTime/2)


def test_convolve_sep3():
    from time import time

    dev = OCLDevice(useDevice=1)
    data  = 10*np.ones((256,)*3,dtype=np.float32)
    hx = np.ones(20)
    hy = 2.*np.ones(20)
    hz = 3.*np.ones(20)

    sumTime = 0
    for dtype in (np.float32,np.uint16):
        t = time()
        out = convolve_sep3(dev,data.astype(dtype),hx,hy,hz)
        sumTime += time()-t
        np.testing.assert_equal(out[40,40,40],data[40,40,40]*np.sum(hx)*np.sum(hy)*np.sum(hz))

    print "convolving %s image with %s kernel (separable) in %.2f ms"%(data.shape,(len(hx),len(hy),len(hz)),1000*sumTime/2)


def test_sizes():
    from time import time

    dev = OCLDevice(useDevice=1)
    data  = 10*np.ones((32,)*3,dtype=np.float32)

    for N in range(25,40,2):
        h = np.ones((N,)*3)
        print "convolving with kernel of size %s " % (h.shape,)
        out = convolve3(dev,data,h)

    h = np.ones((20,)*3)
    for N in range(60,280,30):
        data  = np.ones((N,)*3,dtype=np.float32)
        print "convolving image of size %s " % (data.shape,)
        out = convolve3(dev,data,h)



if __name__ == '__main__':

    # test_convolve2()
    # test_convolve_sep2()
    # test_convolve3()
    # test_convolve_sep3()
    # test_sizes()

    
    dev = OCLDevice(useDevice=1)
    data  = np.ones((240,)*3,dtype=np.float32)


    out = convolve3(dev,data,np.ones((17,)*3))


    # proc = OCLProcessor(dev,absPath("kernels/convolve_kernels.cl"))

    # Nz,Ny,Nx = h.shape
    # hbuf = dev.createBuffer(Nx*Ny*Nz,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)
    # inImg = dev.createImage(data.shape[::-1],dtype = np.float32 )
    # outImg = dev.createImage(data.shape[::-1],dtype = np.float32,mem_flags= cl.mem_flags.READ_WRITE)

    # dev.writeImage(inImg,data)

    # dev.writeBuffer(hbuf,h.astype(np.float32).flatten())

    # proc.runKernel("foo",inImg.shape,(8,8,8),inImg,hbuf,np.int32(Nx),np.int32(Ny),np.int32(Nz),outImg)
