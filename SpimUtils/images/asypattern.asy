

void rods(){

  size(256,256);

  draw(unitsquare);

  srand(0);
  int N = 100;


  for (int i = 0; i < N; ++i){
	real r = .03*(1+unitrand());
  
	fill(shift(unitrand(),unitrand())*rotate(360*unitrand())*scale(r,.3*r)*unitcircle,white);
  }


  shipout("rods",bbox(black,Fill));
}

void stripes(){

  size(256,256);

  draw(shift(-.05,-.05)*scale(1.1)*unitsquare);

  srand(0);
  int N = 12;


  for (int i = 0; i < N; ++i){

	draw((1.*i/(N-1),-.1)--(1.*i/(N-1),1.1),white+5);
  }


  shipout("stripes",bbox(black,Fill));
}

void patterns(){

  size(256,256);

  // draw(shift(-1,-1)*scale(2)*unitsquare);

  srand(0);
  int N = 300;

  fill(shift(0,0)*scale(.2)*unitcircle,white);
  
  for (int i = 0; i < N; ++i){
	real r_0 = sqrt(unitrand());
	real phi = 2pi*unitrand();
	real r = .02*(1+unitrand());
	fill(shift(r_0*(cos(phi),sin(phi)))*scale(r,r)*unitcircle,white);
	fill(shift(r_0*(cos(phi),sin(phi)))*scale(.4*r,.4*r)*unitcircle,black);
  }

  int N2 = 14;
  for (int i = 0; i < N2; ++i){
	path p;
	for (int j = 0; j < N2; ++j){
	  p = p..(.3/N2*unitrand(),3.*j/(N2-1));
	}
	draw(shift(1.+1.*i/N2,-2)*p,white+1);
	
  }

  for (int i = 0; i < N2; ++i){
	path p;
	for (int j = 0; j < N2; ++j){
	  p = p..(3.*j/(N2-1),.3/N2*unitrand());
	}
	draw(shift(-1,-1-1.*i/N2)*p,white+1);
  }

  

  shipout("patterns",bbox(black,Fill));
}



void patterns2(){

  import patterns;
  size(256,256);

  add("crosshatch",crosshatch(4mm,white+2));
  add("hatch",hatch(3mm,white+1));
  add("brick",brick(white+2));
  add("checker",checker(4mm,white));

  path centersquare = shift(-.5,-.5)*unitsquare;
	
  draw(scale(2)*centersquare);

  

  fill(shift(-.5,-.5)*scale(.9)*centersquare, pattern("crosshatch"));
  fill(shift(.5,-.5)*scale(.9)*centersquare, pattern("hatch"));
  fill(shift(-.5,.5)*scale(.9)*centersquare, pattern("checker"));
  fill(shift(.5,.5)*scale(.9)*centersquare, pattern("brick"));

  fill(scale(.5)*centersquare,white);
  shipout("patterns2",bbox(black,Fill));
}



patterns();
