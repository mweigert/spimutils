__kernel void convolve2d_float(__read_only image2d_t input,__global float* h,const int Nx,const int Ny,__write_only image2d_t output){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  int i0 = get_global_id(0);
  int j0 = get_global_id(1);


  float res = 0.f;

  for (int i = 0; i < Nx; ++i){
	  for (int j = 0; j < Ny; ++j){

		float dx = -.5f*(Nx-1)+i;
		float dy = -.5f*(Ny-1)+j;
		
		res += h[i+Nx*j]*read_imagef(input,sampler,(float2)(i0+dx,j0+dy)).x;

	  }
  }
  write_imagef(output,(int2)(i0,j0),(float4)(res,0,0,0));
  

}


__kernel void convolve2d_short(__read_only image2d_t input,__global float* h,const int Nx,const int Ny,__write_only image2d_t output){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  int i0 = get_global_id(0);
  int j0 = get_global_id(1);


  float res = 0.f;

  for (int i = 0; i < Nx; ++i){
	  for (int j = 0; j < Ny; ++j){

		float dx = -.5f*(Nx-1)+i;
		float dy = -.5f*(Ny-1)+j;
		
		res += h[i+Nx*j]*read_imageui(input,sampler,(float2)(i0+dx,j0+dy)).x;

	  }
  }
  write_imageui(output,(int2)(i0,j0),(uint4)(res,0,0,0));
  

}




__kernel void convolve3d_float(__read_only image3d_t input,__global float* h,const int Nx,const int Ny,const int Nz,__write_only image3d_t output){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  int i0 = get_global_id(0);
  int j0 = get_global_id(1);
  int k0 = get_global_id(2);


  float res = 0.f;

  for (int i = 0; i < Nx; ++i){
  	  for (int j = 0; j < Ny; ++j){
  		for (int k = 0; k < Nz; ++k){

  		  float dx = -.5f*(Nx-1)+i;
  		  float dy = -.5f*(Ny-1)+j;
  		  float dz = -.5f*(Nz-1)+k;
		
  		  res += h[i+Nx*j+Nx*Ny*k]*read_imagef(input,sampler,(float4)(i0+dx,j0+dy,k0+dz,0)).x;
  		}
  	  }
  }
  
  write_imagef(output,(int4)(i0,j0,k0,0),(float4)(res,0,0,0));
  

}


__kernel void convolve3d_short(__read_only image3d_t input,__global float* h,const int Nx,const int Ny,const int Nz,__write_only image3d_t output){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  int i0 = get_global_id(0);
  int j0 = get_global_id(1);
  int k0 = get_global_id(2);


  float res = 0.f;

  for (int i = 0; i < Nx; ++i){
	  for (int j = 0; j < Ny; ++j){
		for (int k = 0; k < Nz; ++k){

		  float dx = -.5f*(Nx-1)+i;
		  float dy = -.5f*(Ny-1)+j;
		  float dz = -.5f*(Nz-1)+k;
		
		  res += h[i+Nx*j+Nx*Ny*k]*read_imageui(input,sampler,(float4)(i0+dx,j0+dy,k0+dz,0)).x;
		}
	  }
  }
  
  write_imageui(output,(int4)(i0,j0,k0,0),(uint4)(res,0,0,0));
  

}


// separable versions

__kernel void convolve_sep2d_float(__read_only image2d_t input, __global float * h, const int N,__write_only image2d_t output, const int flag){

  // flag = 1 -> in x axis 
  // flag = 2 -> in y axis 
  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i0 = get_global_id(0);
  uint j0 = get_global_id(1);

  
  const int dx = flag & 1;
  const int dy = (flag&2)/2;

  float res = 0.f;

  for (int i = 0; i < N; ++i){
	float j = i-.5f*(N-1);
    res += h[i]*read_imagef(input,sampler,(float2)(i0+dx*j,j0+dy*j)).x;
  }

  write_imagef(output,(int2)(i0,j0),(float4)(res,0,0,0));
  
}


__kernel void convolve_sep2d_short(__read_only image2d_t input, __global float * h, const int N,__write_only image2d_t output, const int flag){

  // flag = 1 -> in x axis 
  // flag = 2 -> in y axis 
  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i0 = get_global_id(0);
  uint j0 = get_global_id(1);

  
  const int dx = flag & 1;
  const int dy = (flag&2)/2;

  float res = 0.f;

  for (int i = 0; i < N; ++i){
	float j = i-.5f*(N-1);
    res += h[i]*read_imageui(input,sampler,(float2)(i0+dx*j,j0+dy*j)).x;
  }

  write_imageui(output,(int2)(i0,j0),(uint4)(res,0,0,0));
  
}



__kernel void convolve_sep3d_float(__read_only image3d_t input, __global float * h, const int N,__write_only image3d_t output,const int flag){

  // flag = 1 -> in x axis 
  // flag = 2 -> in y axis 
  // flag = 4 -> in z axis 
  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i0 = get_global_id(0);
  uint j0 = get_global_id(1);
  uint k0 = get_global_id(2);

  const int dx = flag & 1;
  const int dy = (flag&2)/2;
  const int dz = (flag&4)/4;

  float res = 0.f;

  for (int i = 0; i < N; ++i){
	float j = i-.5f*(N-1);
	res += h[i]*read_imagef(input,sampler,(float4)(i0+dx*j,j0+dy*j,k0+dz*j,0)).x;
  }

  write_imagef(output,(int4)(i0,j0,k0,0),(float4)(res,0,0,0));
  
}


__kernel void convolve_sep3d_short(__read_only image3d_t input, __global float * h, const int N,__write_only image3d_t output,const int flag){

  // flag = 1 -> in x axis 
  // flag = 2 -> in y axis 
  // flag = 4 -> in z axis 
  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i0 = get_global_id(0);
  uint j0 = get_global_id(1);
  uint k0 = get_global_id(2);

  const int dx = flag & 1;
  const int dy = (flag&2)/2;
  const int dz = (flag&4)/4;

  float res = 0.f;

  for (int i = 0; i < N; ++i){
	float j = i-.5f*(N-1);
	res += h[i]*read_imageui(input,sampler,(float4)(i0+dx*j,j0+dy*j,k0+dz*j,0)).x;
  }

  write_imageui(output,(int4)(i0,j0,k0,0),(uint4)(res,0,0,0));
  
}








__kernel void foo(__read_only image3d_t input,__global float* h,const int Nx,const int Ny,const int Nz,__write_only image3d_t output){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  int i0 = get_global_id(0);
  int j0 = get_global_id(1);
  int k0 = get_global_id(2);


  float res = 0.f;
  write_imagef(output,(int4)(i0,j0,k0,0),(float4)(i0,0,0,0));
  

}
