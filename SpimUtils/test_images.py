import os
from SpimUtils import fromSpimFolder,openImageFile, read3dTiff
from scipy.misc import lena as lena_scipy
import numpy as np
absPath = lambda s: os.path.join(os.path.dirname(__file__),s)



def fluorescent():
    return openImageFile(absPath("images/fluorescent.jpg"))

def barbara():
    return openImageFile(absPath("images/barbara.jpg"))

def histones():
    return openImageFile(absPath("images/histones.png"))

def dots():
    return openImageFile(absPath("images/dots.png"))


def patterns():
    return openImageFile(absPath("images/patterns.png"))


def spim_data():
    return fromSpimFolder(absPath("images/ExampleHistone/"))[0,:,:,:]

def histub():
    return read3dTiff(absPath("images/HisTub.tif"))


def resolution():
    return openImageFile(absPath("images/resolution.png"))

def lena():
    return openImageFile(absPath("images/lena.png"))

def usaf():
    return openImageFile(absPath("images/usaf.png"))

def actin():
    return openImageFile(absPath("images/actin.jpg"))

def chirp():
    return openImageFile(absPath("images/chirp.png"))


def mpi_logo3():
    return read3dTiff(absPath("images/mpi_logo3.tif"))
    
    # x = np.linspace(-1,1,N).astype(np.float32)
    # Z,Y,X = np.meshgrid(x,x,x , indexing = "ij")
    # R = np.sqrt(X**2+Y**2+Z**2)
    # R2 = np.sqrt((X-.4)**2+(Y+.2)**2+Z**2)
    # phi = np.arctan2(Z,Y)
    # theta = np.arctan2(X,np.sqrt(Y**2+Z**2))
    # u = np.exp(-500*(R-1.)**2)*np.sum(np.exp(-150*(-theta-t+.1*(t-np.pi/2.)*
    #                                                np.exp(-np.sin(2*(phi+np.pi/2.))))**2)
    # for t in np.linspace(-np.pi/2.,np.pi/2.,10))*(1+Z)

    # u2 = np.exp(-7*R2**2)
    # return (100*(u + 2*u2))
