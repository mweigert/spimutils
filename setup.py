from setuptools import setup

setup(name='SpimUtils',
      version='0.1',
      description='helper functions for handling spim data',
      url='http://mweigert@bitbucket.org/mweigert/spimutils',
      author='Martin Weigert',
      author_email='mweigert@mpi-cbg.de',
      license='MIT',
      packages=['SpimUtils'],
      install_requires=[
          'numpy', 'Pillow'],
      zip_safe=False)


