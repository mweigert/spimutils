import os
import numpy as np
import re
import itertools
from PIL import Image


def parseIndexFile(fname):
    """
    returns (t,z,y,z) dimensions of a spim stack
    """
    try:
        lines = open(fname).readlines()
    except IOError:
        print "could not open and read ",fname
        return None

    items = lines[0].replace("\t",",").split(",")
    try:
        stackSize = [int(i) for i in items[-4:-1]] +[len(lines)]
    except Exception as e:
        print e
        print "couldnt parse ", fname
        return None
    stackSize.reverse()
    return stackSize


def parseMetaFile(fName):
    """
    returns pixelSizes (dx,dy,dz)
    """

    with open(fName) as f:
        s = f.read()
        try:
            z1 = float(re.findall("StartZ.*",s)[0].split("\t")[2])
            z2 = float(re.findall("StopZ.*",s)[0].split("\t")[2])
            zN = float(re.findall("NumberOfPlanes.*",s)[0].split("\t")[2])

            return (.162,.162, (1.*z2-z1)/zN)
        except Exception as e:
            print e
            print "coulndt parse ", fName
            return (1.,1.,1.)


# def fromSpimFolder(fName,dataFileName="data/data.bin",indexFileName="data/index.txt",count=-1):
#     stackSize = parseIndexFile(os.path.join(fName,indexFileName))
#     if stackSize:
#         if count>0:
#             stackSize[0] = min(count,stackSize[0])
#         print stackSize

#         return np.fromfile(os.path.join(fName,dataFileName),dtype="<u2",count=np.prod(stackSize)).reshape(stackSize)


def fromSpimFolder(fName,dataFileName="data/data.bin",indexFileName="data/index.txt",pos=0,count=1):
    stackSize = parseIndexFile(os.path.join(fName,indexFileName))

    if stackSize:
        # clamp to pos to stackSize
        pos = min(pos,stackSize[0]-1)
        pos = max(pos,0)

        if count>0:
            stackSize[0] = min(count,stackSize[0]-pos)
        else:
            stackSize[0] = max(0,stackSize[0]-pos)

        with open(os.path.join(fName,dataFileName),"rb") as f:
            f.seek(2*pos*np.prod(stackSize[1:]))
            return np.fromfile(f,dtype="<u2",
                               count=np.prod(stackSize)).reshape(stackSize)


def fromSpimFile(fName,stackSize):
    return np.fromfile(fName,dtype="<u2").reshape(stackSize)


def saveAsSpimFolder(dirName,data,stackUnits = (1.,1.,1.), force = True):
    if not os.path.exists(dirName):
        os.makedirs(dirName)
    else:
        if not force:
            print dirname + " already exists and force = False, skipping..."
            return



# def async_prefetch_wrapper(iterable, buffer=3):
#     """
#     wraps an iterater such that it produces items in the background
#     uses a bounded queue to limit memory consumption
#     """
#     def worker(q,it):
#         for item in it:
#             q.put(item)
#         q.put(None)
#     # launch a thread to fetch the items in the background
#     queue = Queue.Queue(buffer)
#     it = iter(iterable)
#     thread = threading.Thread(target=worker, args=(queue, it))
#     thread.daemon = True
#     thread.start()
#     # pull the items of the queue as requested
#     while True:
#         item = queue.get()
#         if item == None:
#             return
#         else:
#             yield item

# def async_prefetch(func):
#     """
#     decorator to make generator functions fetch items in the background
#     """
#     @functools.wraps(func)
#     def wrapper(*args, **kwds):
#         return async_prefetch_wrapper( func(*args, **kwds) )
#     return wrapper


# @async_prefetch
# def iterSpimFolder(fName,dataFileName="data/data.bin",
#                    indexFileName="data/index.txt",
#                    pos=0,count=-1):

#     stackSize = parseIndexFile(os.path.join(fName,indexFileName))
#     if count==-1:
#         count = stackSize[0]
#     count = min(stackSize[0],count)
#     with open(os.path.join(fName,dataFileName),"rb") as f:

#         for i in itertools.cycle(range(pos,pos+count)):
#             f.seek(2*i*np.prod(stackSize[1:]))
#             yield np.fromfile(f,dtype="<u2",count=np.prod(stackSize[1:])).reshape(stackSize[1:])


def createFakeSpimFolder(fName, stackSize= [10,10,32,32], stackUnits = (1,1,1),
                         createData = False):
    if not os.path.exists(fName):
        os.makedirs(fName)
    if not os.path.exists(os.path.join(fName,"data")):
        os.makedirs(os.path.join(fName,"data"))


    Nt,Nz,Ny,Nx = stackSize

    if createData:
        x = np.linspace(-1,1,Nx)
        y = np.linspace(-1,1,Ny)
        z = np.linspace(-1,1,Nz)
        Y,Z,X = np.meshgrid(y,z,x)

        datafName = os.path.join(fName,"data/data.bin")
        with open(datafName,"wa") as f:
            for t in np.linspace(0,1,Nt):
                data = 200*np.exp(-10/(t+1.)**2*((X-t)**2+Y**2+Z**2))
                data.astype(np.uint16).tofile(f)

    indexfName = os.path.join(fName,"data/index.txt")
    with open(indexfName,"w") as f:
        for i in range(Nt):
            f.write("%i\t0.0000\t1,%i,%i,%i\t0\n"%(i,Nx,Ny,Nz))

    metafName = os.path.join(fName,"metadata.txt")
    with open(metafName,"w") as f:
        f.write("timelapse.NumberOfPlanes\t=\t%i\t0\n"%Nz)
        f.write("timelapse.StartZ\t=\t0\t0\n")
        f.write("timelapse.StopZ\t=\t%.2f\t0\n"%(stackUnits[2]*Nz))


def read3dTiff(fName):
    img = Image.open(fName)
    i = 0
    data = []
    while True:
        try:
            img.seek(i)
        except EOFError:
            break
        data.append(np.asarray(img))
        i += 1

    return i


if __name__ == '__main__':
    # createFakeSpimFolder("Fake", createData = True)
    # d = fromSpimFolder("Fake",count=1)
    # print d.shape


    print read3dTiff("../../Tmp/test1.tif")
